import csvtojson from './csvtojson.js';

import Vue from 'vue';

new Vue({
  el: '#app-container',
  data: {
    speakers: [],
    schedule: [],
    topics: [],
    active: undefined,
    key: '2PACX-1vQZ6_JlfKkd9bscJnfIBkhJYleb92Blsy1_vEl_78NrqQ8Q2BfaQBvEHmMzJ_yCMZgdNs6AVOdedTT0',
  },
  computed:{
    filtered(){
      var filtered = this.schedule;
      var vm = this;
      // if(this.filter_venue){
      //   filtered = filtered.filter(function(item){
      //     return item.venue==vm.filter_venue
      //   })
      // }
      // if(this.filter_cat){
      //   filtered = filtered.filter(function(item){
      //     return item.category==vm.filter_cat
      //   })
      // }

      filtered = filtered.slice().sort(function(a, b) {
        var date1 = Date.parse(a.date + " " + a.start);
        var date2 = Date.parse(b.date + " " + b.start);

        return date1 - date2;
      });

      return filtered;
    },
    dates(){
      return this.getColUniq('date');
    },
    patrons(){
      return this.sort('patron');
    },
    chairpersons(){
      return this.sort('chair');
    },
    secretaries(){
      return this.sort('sec');
    },
    scientific(){
      return this.sort('sci');
    },
  },
  methods:{
    getColUniq: function(col){
      var column = this.schedule.map(function(item){
        return item[col];
      })
      return [... new Set(column)];
    },
    dated: function(date){
      return this.filtered.filter(function(item){
        return item.date == date;
      });
    },
    sort: function(typ) {
      var sorted = this.speakers.filter(function(item){
        return item.type == typ;
      })

      // Set slice() to avoid to generate an infinite loop!
      sorted = sorted.slice().sort(function(a, b) {
        return a.order - b.order;
      });

      return sorted.filter(function(item){
        return item.order>0
      })
    },
    loadData: function(item, key, gid){
      var vm = this;
      fetch(`https://docs.google.com/spreadsheets/d/e/${key}/pub?gid=${gid}&single=true&output=csv`)
      .then(response => response.text())
      .then(csv => csvtojson(csv))
      .then(function(json){
        vm[item]= JSON.parse(json);
      });
    },
    changeSched: function(it){
      this.active === it? this.active = undefined : this.active = it;
      document.getElementById('app-container').scrollTo(0,0);
    }
  },
  mounted(){
    this.loadData('schedule', this.key, '0');
    this.loadData('speakers', this.key, '1908240457');
    this.loadData('topics', this.key, '809881798');
  }
})