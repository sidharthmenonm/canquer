import csvtojson from './csvtojson.js';

import Vue from 'vue';

new Vue({
  el: '#app',
  data: {
    speakers: [],
    schedule: [],
    topics: [],
    active: undefined,
    key: '2PACX-1vQZ6_JlfKkd9bscJnfIBkhJYleb92Blsy1_vEl_78NrqQ8Q2BfaQBvEHmMzJ_yCMZgdNs6AVOdedTT0',
  },
  computed:{
    filtered(){
      var filtered = this.schedule;
      var vm = this;
      // if(this.filter_venue){
      //   filtered = filtered.filter(function(item){
      //     return item.venue==vm.filter_venue
      //   })
      // }
      // if(this.filter_cat){
      //   filtered = filtered.filter(function(item){
      //     return item.category==vm.filter_cat
      //   })
      // }

      filtered = filtered.slice().sort(function(a, b) {
        var date1 = Date.parse(a.date + " " + a.start);
        var date2 = Date.parse(b.date + " " + b.start);

        return date1 - date2;
      });

      return filtered;
    },
    dates(){
      return this.getColUniq('date');
    },
    patrons(){
      return this.sort('patron');
    },
    chairpersons(){
      return this.sort('chair');
    },
    secretaries(){
      return this.sort('sec');
    },
    scientific(){
      return this.sort('sci');
    },
  },
  methods:{
    getColUniq: function(col){
      var column = this.schedule.map(function(item){
        return item[col];
      })
      return [... new Set(column)];
    },
    dated: function(date){
      return this.filtered.filter(function(item){
        return item.date == date;
      });
    },
    sort: function(typ) {
      var sorted = this.speakers.filter(function(item){
        return item.type == typ;
      })

      // Set slice() to avoid to generate an infinite loop!
      sorted = sorted.slice().sort(function(a, b) {
        return a.order - b.order;
      });

      return sorted.filter(function(item){
        return item.order>0
      })
    },
    loadData: function(item, key, gid){
      var vm = this;
      fetch(`https://docs.google.com/spreadsheets/d/e/${key}/pub?gid=${gid}&single=true&output=csv`)
      .then(response => response.text())
      .then(csv => csvtojson(csv))
      .then(function(json){
        vm[item]= JSON.parse(json);
      });
    },
  },
  mounted(){
    this.loadData('schedule', this.key, '0');
    this.loadData('speakers', this.key, '1908240457');
    this.loadData('topics', this.key, '809881798');
  }
})


// new Vue({ 
//   el: '#app',
//   data: {
//     data: [],
//     key: '2PACX-1vQ0pFpx1DYMtkTt3wWdMAFN0dH9k84ByQwgkGwfrjnqBKYB-UVGC8GFCjbgpaBsPVXCmfj4DbIPNaG4',
//   },
//   computed: {
//     patrons(){
//       return this.sort('Patrons');
//     },
//     chairpersons(){
//       return this.sort('Chairpersons');
//     },
//     secretaries(){
//       return this.sort('Secretaries');
//     },
//     scientific(){
//       return this.sort('Scientific');
//     },
//     keynote(){
//       return this.sort('Keynote');
//     },
//     session(){
//       return this.sort('Session');
//     },
//     agendas(){
//       return this.data.filter(function(item){
//         return item.topic.length > 0;
//       })
//     }
//   },
//   methods: {
//     sort: function(typ) {
//       var sorted = this.data.filter(function(item){
//         return item.type == typ;
//       })

//       // Set slice() to avoid to generate an infinite loop!
//       sorted = sorted.slice().sort(function(a, b) {
//         return a.order - b.order;
//       });

//       return sorted.filter(function(item){
//         return item.order>0
//       })
//     },
//     loadData: function(item, key, gid){
//       var vm = this;
//       fetch(`https://docs.google.com/spreadsheets/d/e/${key}/pub?gid=${gid}&single=true&output=csv`)
//       .then(response => response.text())
//       .then(csv => csvtojson(csv))
//       .then(function(json){
//         vm[item]= JSON.parse(json);
//       });
//     },
//   },
//   mounted(){
//     this.loadData('data', this.key, '0');
//   }
// })